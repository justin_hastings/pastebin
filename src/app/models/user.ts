export interface User {
  uid: string;
  email: string | null;
  notes?: any[];
  photoURL?: string | null;
  displayName?: string | null;
  myCustomData?: string | null;
  role?: 'user' | 'admin';
}
