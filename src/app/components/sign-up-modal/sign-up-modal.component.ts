import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  AbstractControl,
  FormControl,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-up-modal',
  templateUrl: './sign-up-modal.component.html',
  styleUrls: ['./sign-up-modal.component.scss'],
})
export class SignUpModalComponent implements OnInit {
  constructor(private AUTH: AuthService, private ROUTER: Router) {}

  email: FormControl = new FormControl('', Validators.email);
  password: FormControl = new FormControl(
    '',
    Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
  );
  confirmPassword: FormControl = new FormControl('');
  name: FormControl = new FormControl('', Validators.minLength(1));

  error: string = '';

  close = new BehaviorSubject<boolean>(false);

  hidePassword = true;
  hidePassword2 = true;

  ngOnInit(): void {
    this.email.valueChanges.subscribe(() => {
      this.error = '';
    });

    combineLatest([
      this.password.valueChanges,
      this.confirmPassword.valueChanges,
    ]).subscribe(([a, b]) => {
      if (a == b) {
        this.confirmPassword.setErrors(null);
      } else {
        this.confirmPassword.setErrors({ match: true });
      }
    });
  }

  signUp() {
    this.AUTH.emailSignup(this.email.value, this.password.value)
      .then((status) => {
        this.close.next(true);
        this.ROUTER.navigate(['/notepad']);
      })
      .catch((error) => {
        this.email.setErrors({ firestore: true });
        this.error = error.message;
      });
  }
}
