import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlDirective, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SignUpModalComponent } from '../sign-up-modal/sign-up-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private AUTH: AuthService,
    private DIALOG: MatDialog,
    private ROUTER: Router
  ) {}

  email: FormControl = new FormControl('', Validators.email);
  password: FormControl = new FormControl(
    '',
    Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
  );
  passwordError = 'The email & password does not match';
  emailError =
    'A user does not exits under this email. Create an account below or continue as a guest.';
  hidePassword = true;

  ngOnInit(): void {
    this.AUTH.$user.subscribe((user) => {
      if (user) {
        this.ROUTER.navigate(['/notepad']);
      }
    });
  }

  loginWithGoogle() {
    this.AUTH.googleSignin();
  }

  loginWithEmail() {
    this.AUTH.emailSignin(this.email.value, this.password.value)
      .then(() => {
        this.ROUTER.navigate(['/notepad']);
      })
      .catch((error) => {
        this.setFormFirestoreError(error, this.email, 'user-not-found');
        this.setFormFirestoreError(error, this.password, 'wrong-password');
      });
  }

  setFormFirestoreError(error: any, form: FormControl, code: string) {
    if (error.code.includes(code)) {
      form.setErrors({ code: true });
      this.resetFirestoreError(form);
    }
  }

  resetFirestoreError(form: FormControl) {
    form.valueChanges.pipe(take(1)).subscribe(() => {
      form.setErrors({});
    });
  }

  signOut() {
    this.AUTH.signOut();
  }

  openSignUp() {
    let ref = this.DIALOG.open(SignUpModalComponent);
    ref.componentInstance.close.subscribe((close) => {
      if (close) {
        ref.close();
      }
    });
  }
}
