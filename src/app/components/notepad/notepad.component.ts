import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { AuthService } from 'src/app/services/auth/auth.service';
import firebase from 'firebase/app';
import { MatInput } from '@angular/material/input';

export interface Note {
  userId: string;
  privacy: string;
  note: string;
  title: string;
}

@Component({
  selector: 'app-notepad',
  templateUrl: './notepad.component.html',
  styleUrls: ['./notepad.component.scss'],
})
export class NotepadComponent implements OnInit {
  constructor(
    private FIRESTORE: AngularFirestore,
    private AUTH: AuthService,
    private ROUTER: ActivatedRoute,
    private ROUTE: Router
  ) {}

  privacyOption = 'public';
  note = new FormControl('');
  noteTitle = new FormControl('');
  noteUrlControl = new FormControl(location.href);
  noteId = '';
  noteUserId = '';
  noteFromFirestore = '';
  noteTitleFromFirestore = '';
  saveView = false;
  editView = true;
  myNote = false;
  noteUrl = location.href;

  ngOnInit(): void {
    this.noteUrlControl.disable();
    this.ROUTER.queryParams.subscribe((params) => {
      if (params.note) {
        this.FIRESTORE.collection('notes')
          .doc(params.note)
          .get()
          .subscribe((doc) => {
            if (doc.data()) {
              let data = doc.data() as Note;
              this.setData(data, params.note);
            } else {
              this.ROUTE.navigate(['/notepad']);
            }
          });
      } else {
        this.note.patchValue('');
        this.noteTitle.patchValue('');
        this.editView = true;
        this.saveView = false;
      }
    });
    this.AUTH.$user.subscribe((user) => {
      if (user) {
        this.myNote = user.uid === this.noteUserId;
      }
    });
  }

  setData(data: Note, id: any) {
    this.note.setValue(data.note);
    this.noteFromFirestore = data.note;
    this.noteTitle.setValue(data.title);
    this.noteTitleFromFirestore = data.title;
    this.editView = false;
    this.saveView = true;
    this.noteId = id;
    this.noteUserId = data.userId;
  }

  onSumbit(existing: boolean) {
    let id = existing ? this.noteId : UUID.UUID();
    let note: any = {
      note: this.note.value,
      title: this.noteTitle.value,
      noteId: id,
      // privacy: this.privacyOption,
    };
    this.FIRESTORE.collection('notes')
      .doc(id)
      .set(note, { merge: true })
      .then(() => {
        location.href += '?note=' + id;
        // if (this.myNote) {
        if (this.AUTH.userId) {
          this.FIRESTORE.collection('notes').doc(id).set(
            {
              userId: this.AUTH.userId,
            },
            { merge: true }
          );
          this.FIRESTORE.collection('users')
            .doc(this.AUTH.userId)
            .update({ notes: firebase.firestore.FieldValue.arrayUnion(id) });
        }
        // }
      });
  }

  enableEdit() {
    this.editView = true;
  }
}
