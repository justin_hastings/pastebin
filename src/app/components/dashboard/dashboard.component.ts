import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(private FIRESTORE: AngularFirestore, private ROUTER: Router) {}

  notes: any[] = [];

  @ViewChild('page', { static: false }) paginator: MatPaginator | undefined;

  ngAfterViewInit() {
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
  }

  ngOnInit(): void {
    this.FIRESTORE.collection('notes')
      .get()
      .subscribe((docs) => {
        docs.forEach((doc) => {
          let data = doc.data() as any;
          this.notes.push({ title: data.title, noteId: data.noteId });
        });
        this.dataSource = new MatTableDataSource(this.notes);
        if (this.paginator) {
          this.dataSource.paginator = this.paginator;
        }
      });
  }

  openNote(noteId: any) {
    this.ROUTER.navigate(['/notepad'], { queryParams: { note: noteId } });
  }

  displayedColumns: string[] = ['title'];
  dataSource = new MatTableDataSource([{ title: '' }]);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
