import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-my-notes',
  templateUrl: './my-notes.component.html',
  styleUrls: ['./my-notes.component.scss'],
})
export class MyNotesComponent implements OnInit {
  constructor(
    private AUTH: AuthService,
    private FIRESTORE: AngularFirestore,
    private ROUTER: Router
  ) {}

  notes: any[] = [];

  ngOnInit(): void {
    this.FIRESTORE.doc(`users/${this.AUTH.userId}`)
      .get()
      .subscribe((user) => {
        let userData = user.data() as User;
        userData.notes?.forEach((note) => {
          this.FIRESTORE.doc(`notes/${note}`)
            .get()
            .subscribe((noteRef) => {
              let data: any = noteRef.data();
              if (data) {
                data['id'] = note;
                this.notes.push(data);
              }
            });
        });
      });
  }

  onClick(note: any) {
    this.ROUTER.navigate(['/notepad'], { queryParams: { note: note.id } });
  }
}
