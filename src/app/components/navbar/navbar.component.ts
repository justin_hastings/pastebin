import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  constructor(private AUTH: AuthService) {}

  $user = this.AUTH.$user;

  displayName = '';

  ngOnInit(): void {
    this.$user.subscribe((user) => {
      if (user) {
        if (user.displayName) {
          this.displayName = user.displayName;
        } else {
          this.displayName = user?.email ? user?.email : '';
        }
      } else {
        this.displayName = 'Guest';
      }
    });
  }

  logout() {
    this.AUTH.signOut();
  }
}
