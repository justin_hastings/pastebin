import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import firebase from 'firebase/app';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  $user: Observable<User | null | undefined>;
  constructor(
    private AF_AUTH: AngularFireAuth,
    private FIRESTORE: AngularFirestore,
    private ROUTER: Router
  ) {
    // Get the auth state, then fetch the Firestore user document or return null
    this.$user = this.AF_AUTH.authState.pipe(
      switchMap((user) => {
        // Logged in
        if (user) {
          this.userId = user.uid;
          return this.FIRESTORE.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          this.userId = null;
          // Logged out
          return of(null);
        }
      })
    );

    this.FIRESTORE.collection('users')
      .get()
      .subscribe((users) => {
        this.allUsers = users.docs.map((doc) => {
          let data = doc.data() as User;
          return data.email;
        });
      });
  }

  userId: any = null;
  allUsers: any[] = [];

  async googleSignin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    const credential = await this.AF_AUTH.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async emailSignin(email: string, password: string) {
    const credential = await this.AF_AUTH.signInWithEmailAndPassword(
      email,
      password
    );
    return this.updateUserData(credential.user);
  }

  async emailSignup(email: string, password: string) {
    await this.AF_AUTH.createUserWithEmailAndPassword(email, password);
    return this.emailSignin(email, password);
  }

  private updateUserData(user: firebase.User | null) {
    // Sets user data to firestore on login
    if (user) {
      const userRef: AngularFirestoreDocument<User> = this.FIRESTORE.doc(
        `users/${user.uid}`
      );

      let data: User = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
      };

      return userRef.set(data, { merge: true });
    }

    return null;
  }

  async signOut() {
    await this.AF_AUTH.signOut();
    this.ROUTER.navigate(['/']);
  }
}
